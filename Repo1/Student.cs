﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo1
{
    class Student
    { 
        public int Id { get; private set; }
        public string Name { get; private set; }
        public string Surname { get; private set; }
        public DateTime Birthdate { get; private set; }
        public Enum Sex { get; private set; }

        public Student(int id, string name, string surname, DateTime birthdate, Enum sex)
        {
            Id = id;
            Name = name;
            Surname = surname;
            Sex = sex;
            Birthdate = birthdate;
        }

    }
}