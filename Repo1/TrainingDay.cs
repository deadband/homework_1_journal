﻿using System;
using System.Collections.Generic;

namespace Repo1
{
    class TrainingDay
    {
        public DateTime TrainingDate { get; private set; }
        private Dictionary<int, int> _presence; //zapisywanie obecnosci, jako t-key numery id studentow

        public TrainingDay(DateTime trainingdate)
        {
            TrainingDate = trainingdate;
            _presence = new Dictionary<int, int>();
        }

        public void SetAttendace(int[] ids)
        {
            foreach (var id in ids)
            {
                Console.Write($"Student ID = {id}. Enter 0-absent, 1-present: ");
                var isPresent = VerificationMethods.VerifyPresence(Console.ReadLine());               
                _presence.Add(id,isPresent);
            }
        }

        public int GetAttendance(int id)
        {
            return _presence[id];
        }
    }
}
