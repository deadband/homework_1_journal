﻿using System;
using System.Collections.Generic;

namespace Repo1
{
    class HomeworkResults
    {
        public int MaxScore { get; private set; }
        private Dictionary<int, int> _results; //zapisywanie wynikow zadan, jako t-key numery id studentow

        public HomeworkResults(int maxscore)
        {
            MaxScore = maxscore;
            _results = new Dictionary<int, int>();
        }

        public void SetResults(int[] ids)
        {
            foreach (var id in ids)
            {
                Console.Write($"Student ID = {id}. Type homework result (0-{MaxScore}): ");
                var score = VerificationMethods.VerifyInteger(Console.ReadLine(), 0, MaxScore);
                _results.Add(id, score);
            }
        }

        public int GetResult(int id)
        {
            return _results[id];
        }
    }
}
