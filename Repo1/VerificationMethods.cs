﻿using System;

namespace Repo1
{
    class VerificationMethods
    {
        const int maxJournalsInFreeVersion = 5;

        public static DateTime VerifyDate(string date)
        {
            var parsePossible = DateTime.TryParse(date.Replace('-', ','), out DateTime result);
            while (!parsePossible)
            {
                Console.Write("Incorrect date format provided. Enter date in YYYY-mm-dd format: ");
                date = Console.ReadLine().Replace('-', ',');
                parsePossible = DateTime.TryParse(date, out result);
            }
            return result;
        }

        public static int VerifyInteger(string number, int min, int max)
        {
            var parsePossible = int.TryParse(number, out int result);
            while (!parsePossible || result > max || result < min)
            {
                Console.Write("Incorrect number entered. Type again: ");
                number = Console.ReadLine();
                parsePossible = int.TryParse(number, out result);
            }
            return result;
        }

        public static Sex VerifySex(string sex)
        {
            while (!(sex == "0" || sex == "1" || sex == "2"))
            {
                Console.Write("Incorrect format! Type 0-Male, 1-Female, 2-No answear: ");
                sex = Console.ReadLine();
            }
            return (Sex)Convert.ToByte(sex);
        }

        public static int VerifyPresence(string presence)
        {
            while (!(presence == "0" || presence == "1"))
            {
                Console.Write("Incorrect format! Enter 0-absent, 1-present: ");
                presence = Console.ReadLine();
            }
            return Convert.ToInt32(presence);
        }

        public static bool VerifyNumberOfJournalsCreated(int numberOfJournals, int selection)
        {        
            if (numberOfJournals == 0 && selection > 1 && selection < 6)
            {
                Console.WriteLine("Create journal first! Press any key to continue...");
                Console.ReadLine();
                return true;
            }

            if (numberOfJournals == 1 && selection == 5)
            {
                Console.WriteLine("There is only one journal created. Can not change selection.");
                Console.ReadLine();
                return true;
            }

            if (numberOfJournals == maxJournalsInFreeVersion && selection == 1)
            {
                Console.WriteLine("In this verison you can create up to 5 journals. Please pay 1000$ to unlock unlimited journals.");
                Console.WriteLine("Press any key to continue...");
                Console.ReadLine();
                return true;
            }

            return false;
        }
    }
}
