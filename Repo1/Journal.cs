﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Repo1
{
    class Journal
    {
        private List<TrainingDay>     _trainingDayList;
        private List<Student>         _studentList;
        private List<HomeworkResults> _homeworkResultsList;    
        public int StudentCount             { get; private set; }
        public string   JournalsName        { get; private set; }
        public string   LeadersName         { get; private set; }
        public DateTime StartDate           { get; private set; }
        public int      AttendanceThreshold { get; private set; }
        public int      HomeworkThreshold   { get; private set; }

        public Journal()
        {
            _studentList            = new List<Student>();
            _trainingDayList        = new List<TrainingDay>();
            _homeworkResultsList    = new List<HomeworkResults>();
            
        }

        public void SetJournal()
        {
            Console.WriteLine();
            Console.Write("Enter journal's name: ");
            var trainingName = Console.ReadLine();
            Console.Write("Enter training leader's name: ");
            var leadersName = Console.ReadLine();
            Console.Write("Enter dash-seperated training start date in YYYY-mm-dd format: ");
            var date = VerificationMethods.VerifyDate(Console.ReadLine());
            Console.Write("Enter homework threshold 0-100 [percent]: ");
            var homework = VerificationMethods.VerifyInteger(Console.ReadLine(), 0, 100);
            Console.Write("Enter attendance threshold 0-100 [percent]: ");
            var attendance = VerificationMethods.VerifyInteger(Console.ReadLine(), 0, 100);
            Console.Write("Enter number of students (1-25): ");
            var numberOfStudents = VerificationMethods.VerifyInteger(Console.ReadLine(), 1, 25);
            Console.WriteLine();
            JournalsName = trainingName;
            LeadersName = leadersName;
            StartDate = date;
            AttendanceThreshold = attendance;
            HomeworkThreshold = homework;
            StudentCount = numberOfStudents;
        }

        public void AddStudent()
        {
            Console.Write("Enter student's ID: ");
            var id = VerificationMethods.VerifyInteger(Console.ReadLine(), 1, int.MaxValue);
            while (_studentList.SingleOrDefault(x => x.Id == id) != null)
            {
                        Console.Write("Student with such an ID already exist, type new ID: ");
                        id = VerificationMethods.VerifyInteger(Console.ReadLine(), 0, int.MaxValue);
            }
            Console.Write("Enter student's name: ");
            var name = Console.ReadLine();
            Console.Write("Enter student's surname: ");
            var surname = Console.ReadLine();
            Console.Write("Enter dash-seperated student's birthday in YYYY-mm-dd format: ");
            var birthdate = VerificationMethods.VerifyDate(Console.ReadLine());
            Console.Write("Enter student's sex (0-Male, 1-Female, 2-No answear): ");
            var sex = VerificationMethods.VerifySex(Console.ReadLine());
            _studentList.Add(new Student(id, name, surname, birthdate, sex));
            Console.WriteLine();
            Console.WriteLine($"Student added. Id = {id}.");
            Console.WriteLine("Press any key to continue...");
            Console.ReadLine();
        }


        public void AddTrainingDay(DateTime trainingdate)
        {
            while (trainingdate<StartDate)
            {
                Console.WriteLine($"Incorrect date! Training day must be after {StartDate.ToShortDateString()}");
                Console.Write("Type dash - seperated training date in YYYY-mm-dd format: ");
                trainingdate = VerificationMethods.VerifyDate(Console.ReadLine());
            }

            for (int i = 0; i < _trainingDayList.Count; i++)
            {
                if (_trainingDayList[i].TrainingDate == trainingdate)
                {
                    Console.WriteLine("This training date already exists. ");
                    Console.Write("Type dash - seperated training date in YYYY-mm-dd format: ");
                    trainingdate = VerificationMethods.VerifyDate(Console.ReadLine());
                    i = 0;
                }
            }

            var trainingDay = new TrainingDay(trainingdate);
            _trainingDayList.Add(trainingDay);

            var studentsIds = _studentList.Select(x => x.Id).ToArray();
            trainingDay.SetAttendace(studentsIds);
            Console.WriteLine();
            Console.WriteLine($"Training day {trainingdate.ToShortDateString()} attendance successfully added");
            Console.WriteLine("Press any key to continue...");
            Console.ReadLine();
        }

        public void AddHomeworkResults(int maxscore)
        {
            var homework = new HomeworkResults(maxscore);
            _homeworkResultsList.Add(homework);
            var studentsIds = _studentList.Select(x => x.Id).ToArray();
            homework.SetResults(studentsIds);
            Console.WriteLine();
            Console.WriteLine($"Homework successfully added");
            Console.WriteLine("Press any key to continue...");
            Console.ReadLine();
        }

        public void JournalRaportStatusDisplay()
        {
            Console.WriteLine();
            Console.WriteLine($"Journal's name:       {JournalsName}");
            Console.WriteLine($"Start date:           {StartDate.ToShortDateString()}");
            Console.WriteLine($"Attendance Treshold:  {AttendanceThreshold}");
            Console.WriteLine($"Homework Treshold:    {AttendanceThreshold}");
            Console.WriteLine($"Number od students:   {StudentCount}");
            Console.WriteLine();
            if (_homeworkResultsList.Count == 0)
            {
                Console.WriteLine("Homework results are not being displayed. There were no homeworks.");
                Console.WriteLine();
            }
            if (_trainingDayList.Count == 0)
            {
                Console.WriteLine("Attendance results are not being displayed. There were no training days added.");
                Console.WriteLine();
            }
        }

        public void JournalRaportResultsDisplay()
        { 
            foreach (var student in _studentList)
            {
                Console.WriteLine($"Student: {student.Name} {student.Surname}");
                var passed = String.Empty;
                if (_trainingDayList.Count>0)
                {
                    var attendance = 0;
                    foreach (var presence in _trainingDayList)
                    {
                        attendance += presence.GetAttendance(student.Id);
                    }
                    var attendancePercent = Math.Round((double)attendance / _trainingDayList.Count * 100, 1);
                    passed = attendancePercent >= AttendanceThreshold ? "Passed" : "Not Passed";
                    Console.WriteLine($"Attendace: {attendance} / {_trainingDayList.Count}      ({attendancePercent}%) {passed}");
                }

                if (_homeworkResultsList.Count>0)
                {
                    var homeworkTotal = 0;
                    var homeworkTotalMax = 0;
                    foreach (var homework in _homeworkResultsList)
                    {
                        homeworkTotal += homework.GetResult(student.Id);
                        homeworkTotalMax += homework.MaxScore;
                    }
                    var homeworkPercent = Math.Round((double)homeworkTotal / homeworkTotalMax * 100, 1);
                    passed = homeworkPercent >= HomeworkThreshold ? "Passed" : "Not Passed";
                    Console.WriteLine($"Homework:  {homeworkTotal} / { homeworkTotalMax}     ({homeworkPercent}%) {passed}");
                }
            }
        }
    }
}