﻿using System;

namespace Repo1
{
    class Display
    {
        public static void MainMenu(string journalselected)
        {
            Console.Clear();
            Console.WriteLine($"Date: {DateTime.Now.ToShortDateString()}, Jurnal app by Bartlomiej Grzywacz v1.0");
            Console.WriteLine();
            Console.WriteLine($"Selected Journal: {journalselected}");
            Console.WriteLine();
            Console.WriteLine("1. Add journal.");
            Console.WriteLine("2. Add training day and check attendance.");
            Console.WriteLine("3. Add homework");
            Console.WriteLine("4. Print current journal report");
            Console.WriteLine("5. Change selected journal");
            Console.WriteLine("6. Exit program");
        }

        public static void ReportFinished()
        {
            Console.WriteLine();
            Console.WriteLine($"Raport finished at {DateTime.Now}");
            Console.WriteLine("Press any key to continue...");
            Console.ReadLine();
        }
    }
}
