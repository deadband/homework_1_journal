﻿using System;
using System.Collections.Generic;

namespace Repo1
{
    public class Program
    {
        static void Main(string[] args)
        {
            var journalList = new List<Journal>();
            var selection = 0;
            var journalSelected = -1;
            
            while (selection!=6)
            {
                var journalselectedString = journalSelected == -1 ? "No selection" : journalList[journalSelected].JournalsName;
                Display.MainMenu(journalselectedString);
                selection = VerificationMethods.VerifyInteger(Console.ReadLine(), 1, 6);

                switch (selection)
                {
                    case 1:
                        if (VerificationMethods.VerifyNumberOfJournalsCreated(journalList.Count,selection)) break;
                        journalList.Add(new Journal());
                        journalSelected = journalList.Count - 1;
                        journalList[journalSelected].SetJournal();

                        for (int i = 0; i < journalList[journalSelected].StudentCount; i++)
                        {
                            journalList[journalSelected].AddStudent();
                        }
                        Console.WriteLine($"New journal {journalList[journalSelected].JournalsName} starting at {journalList[journalSelected].StartDate.ToShortDateString()} was created and selected.");
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadLine();
                        break;

                    case 2:
                        if (VerificationMethods.VerifyNumberOfJournalsCreated(journalList.Count, selection)) break;
                        Console.WriteLine();
                        Console.Write("Enter dash - seperated training date in YYYY-mm-dd format: ");
                        var trainingDate = VerificationMethods.VerifyDate(Console.ReadLine());
                        journalList[journalSelected - 1].AddTrainingDay(trainingDate);
                        break;

                    case 3:
                        if (VerificationMethods.VerifyNumberOfJournalsCreated(journalList.Count,selection)) break;
                        Console.WriteLine();
                        Console.Write("Enter homework's max score:");
                        var maxscore = VerificationMethods.VerifyInteger(Console.ReadLine(), 1, int.MaxValue);
                        journalList[journalSelected].AddHomeworkResults(maxscore);                     
                        break;

                    case 4:
                        if (VerificationMethods.VerifyNumberOfJournalsCreated(journalList.Count,selection)) break;
                        journalList[journalSelected].JournalRaportStatusDisplay();
                        journalList[journalSelected].JournalRaportResultsDisplay();
                        Display.ReportFinished();
                        break;

                    case 5:
                        if (VerificationMethods.VerifyNumberOfJournalsCreated(journalList.Count,selection)) break;
                        Console.WriteLine($"Enter which journal to select (1-{journalList.Count}): ");
                        for (int i = 0; i < journalList.Count; i++)
                        {
                            Console.WriteLine($"{i + 1} - {journalList[i].JournalsName}");
                        }
                        journalSelected = VerificationMethods.VerifyInteger(Console.ReadLine(), 1, journalList.Count) -1;
                        break;
                }
            }
        }
    }
}